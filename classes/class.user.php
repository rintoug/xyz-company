<?php
/**
 *
 * @author      Rinto George
 * @version     1.1
 */
class user {

    /**
     * @param $post
     */
     
    public function checkAuthentication($post) {
		
		$sql = "SELECT * FROM users WHERE username=:username LIMIT 1";
		$params = array('username'=>$post['username']);
		$result = database::fetchRecord($sql,$params);
		if(!empty($result)) {
			if(password_verify($post['password'], $result['password'])) {
				$_SESSION['user_id'] = $result['id'];
				$_SESSION['user_name'] = $result['username'];
				$_SESSION['type'] = $result['type'];
				header("Location:profile.php");
			    exit;
			}
			else {
				header("Location:login.php?msg=Wrong login");
			    exit;
			}
		}
		else {
			header("Location:login.php?msg=Wrong login");
			exit;
		}
		
	}

    /**
     * @param $post
     * @param $files
     */
	
	public function update($post,$files) {
		
		$image='';
		if(!empty($files)) {
			$image = $this->uploadPhoto();
		}
		
		$sql = "UPDATE  users SET nickname=:nickname,firstname=:firstname,lastname=:lastname,profile_image=:profile_image,bgcolor=:bgcolor WHERE id=:id";
		$params = array(
						':nickname'=>$post['nickname'],
						':firstname'=>$post['firstname'],
						':lastname'=>$post['lastname'],
						':id'=>$_SESSION['user_id'],
						':profile_image'=>$image,
						':bgcolor'=>$post['bgcolor'],
						
		                    
				);
		$result = database::save($sql,$params);

		
	}

    /**
     * @return string
     */
	
	public function uploadPhoto() {
		$target_dir = "uploads/";
		
		$filename = basename($_FILES["display_pic"]["name"]);
		$target_file = $target_dir . $filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if (move_uploaded_file($_FILES["display_pic"]["tmp_name"], $target_file)) {
      	  return $filename;
		} else {
		  return '';
		}
	}

    /**
     * @return array
     */
	
	public function getAll() {
		$sql = "SELECT * FROM users";
		$result = database::fetchRecords($sql);
		return $result;
	}

    /**
     * @return array
     */
	function getOne() {
		$sql = "SELECT * FROM users WHERE id=:id";
		$params = array(':id'=>$_SESSION['user_id']);
		$result = database::fetchRecord($sql,$params);
		return $result;
	}

    /**
     * @return array
     */
	
	function topSales() {
		$sql = "SELECT sales.*,users.username,users.type FROM sales LEFT JOIN users ON sales.user_id=users.id ORDER BY amount desc";
		$result = database::fetchRecords($sql);
		return $result;
	}
	
	
     
}
?>