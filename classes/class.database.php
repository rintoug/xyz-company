<?php
/**
 *
 * @author      Rinto George
 * @version     1.1
 */
class database {
     
    private $dbName;
	private $dbHost;
	private $dbPass;
	private $dbUser;
    private static $instance = null;

    /**
     * database constructor.
     */
    private function __construct() {
        // Please note that this is Private Constructor
        $this->dbName = 'incentive_db';
        $this->dbHost = 'localhost';
        $this->dbUser = 'root';
        $this->dbPass = '';
 
        // Your Code here to connect to database //
        $this->dbh = new PDO('mysql:host='.$this->dbHost.';dbname='.$this->dbName, $this->dbUser, $this->dbPass);
		$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    $this->dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    /**
     * @param array $dbDetails
     * @return database|null
     */
     
    public static function connect($dbDetails = array()) {
         
        // Check if instance is already exists      
        if(self::$instance == null) {
            self::$instance = new database();
        }
        return self::$instance;
         
    }

    /**
     * @param $sql
     * @param string $params
     * @return array
     */
	public static function fetchRecords($sql,$params='') {
		$sth = self::$instance->dbh->prepare($sql);
		if(!empty($params)){
			foreach($params as $key=>$value) {
				$sth->bindValue($key, $value);
			}
		}
		$sth->execute();
		return $sth->fetchAll(PDO::FETCH_ASSOC);
	}

    /**
     * @param $sql
     * @param string $params
     * @return array
     */
	public static function fetchRecord($sql,$params='') {
		$sth = self::$instance->dbh->prepare($sql);
		if(!empty($params)){
			foreach($params as $key=>$value) {
				$sth->bindValue($key, $value);
			}
		}
		$sth->execute();
		return $sth->fetch(PDO::FETCH_ASSOC);
	}

    /**
     * @param $sql
     * @param string $params
     */
	
	public static function save($sql,$params='') {
		$sth = self::$instance->dbh->prepare($sql);
		if(!empty($params)){
			foreach($params as $key=>$value) {
				$sth->bindValue($key, $value);
			}
		}
		$sth->execute();
	}
     
}
?>