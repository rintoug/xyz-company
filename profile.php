<?php
require_once "settings.php";
$db = database::connect();

if(empty($_SESSION['user_id'])) {
  header("Location:login.php");
  exit;
}

$user = new user();

$errors='';
if(!empty($_POST)) {
	$is_valid = GUMP::is_valid($_POST, array(
		'firstname' => 'required',
		'lastname' => 'required',
	));
	if($is_valid === true) {
		$update = $user->update($_POST,$_FILES);
    // continue
	} else {
		$errors = $is_valid;
	}
}

$currentUser = $user->getOne();
?>
<?php require_once "header.php"?>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="#">XYZ Project</a> </div>
    <div id="navbar" class="navbar-collapse collapse">

      <form class="navbar-form navbar-right">
        <input type="text" class="form-control" placeholder="Search...">
      </form>
    </div>
  </div>
</nav>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
      <ul class="nav nav-sidebar">
        <li><a href="profile.php">My Profile</a></li>
        <li><a href="top_sales.php">Top Sales</a></li>
        <li><a href="logout.php">Logout</a></li>
      </ul>
    </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="background:<?php print $currentUser['bgcolor']?>">
      <h1 class="page-header">My Profile</h1>
      <form method="post" enctype="multipart/form-data">
        <?php if(!empty($errors)):?>
        <?php foreach($errors as $error):?>
        <?php print "<br>";?>
        <?php print $error?>
        <?php endforeach;?>
        <?php endif;?>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" class="form-control" id="username" name="username" placeholder="username" value="<?php print $currentUser['username']?>">
        </div>
        <div class="form-group">
          <label for="nickname">Nickname</label>
          <input type="text" class="form-control" id="nickname"  name="nickname" placeholder="nickname" value="<?php print $currentUser['nickname']?>">
        </div>
        <div class="form-group">
          <label for="email">Firstname</label>
          <input type="text" class="form-control" id="firstname"  name="firstname" placeholder="firstname" value="<?php print $currentUser['firstname']?>">
        </div>
        <div class="form-group">
          <label for="email">Lastname</label>
          <input type="text" class="form-control" id="lastname" name="lastname" placeholder="lastname" value="<?php print $currentUser['lastname']?>">
        </div>
        <div class="form-group">
          <label for="bgcolor">BGcolor</label>
          <input type="text" class="form-control" id="bgcolor" name="bgcolor" placeholder="bgcolor" value="<?php print $currentUser['bgcolor']?>">
          <i>Note:Input your hex code including #.</i>
        </div>
        <div class="form-group">
          <label for="type">Type</label>
          <select name="type" class="form-control">
            <option value="c" <?php if($currentUser['type']=='c'):?>selected <?php endif;?>>Company</option>
            <option value="p" <?php if($currentUser['type']=='p'):?>selected <?php endif;?>>Individual</option>
          </select>
        </div>
        <div class="form-group">
          <label for="password">Display Picture</label>
          <input type="file" name="display_pic" >
          <?php if(!empty($currentUser['profile_image'])):?>
          <img src="uploads/<?php print $currentUser['profile_image']?>" width="100" >
          
          <br>
          <i>Note:Select another pic to update current pic.</i>
          <?php endif;?>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
</body>
</html>
