<?php
session_start();
define('BASE_DIR',dirname(__FILE__));
function autoloadClasses($className) {
    $filename = BASE_DIR."/classes/class." . strtolower($className) . ".php";
    if (is_readable($filename)) {
        require $filename;
    }
}

spl_autoload_register("autoloadClasses");