<?php
require_once "settings.php";
$db = database::connect();


$errors='';
if(!empty($_POST)) {
	//validatiion
	$is_valid = GUMP::is_valid($_POST, array(
		'username' => 'required|alpha_numeric',
		'password' => 'required|max_len,100|min_len,6'
	));
	if($is_valid === true) {
		$user = new user();
		if($user->checkAuthentication($_POST)) {
			header("Location:profile.php");
			exit;
		}
    // continue
	} else {
		$errors = $is_valid;
	}
}

?>
<?php require_once "header.php"?>

<body>
<div class="container">
  <form class="form-signin" method="post">
  
    <?php if(!empty($errors)):?>
    <?php foreach($errors as $error):?>
	<?php print "<br>";?>
	<?php print $error?>
	<?php endforeach;?>
    <?php endif;?>
    
    <h2 class="form-signin-heading">Login</h2>
    <label for="inputEmail" class="sr-only">username</label>
    <input type="text" id="inputEmail" class="form-control" placeholder="Username" name="username" required autofocus>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required>
    <div class="checkbox"> </div>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
  </form>
</div>
<!-- /container -->

</body>
</html>
