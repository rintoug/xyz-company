<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">
<title>Grid Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="assets/css/custom.css" rel="stylesheet">
<link href="assets/css/carousel.css" rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="assets/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<div class="container">
  <header class="header">
    <div class="col-md-8"><a href="" class="logo"><img src="assets/images/logo.png" boder="0"  width="200"></a></div>
    <div class="col-md-4">
      <div class="social">
        <div class="row">
          <div class="col-md-6"><span class="social-text">Get social with:</span></div>
          <div class="col-md-6 row">
            <ul>
              <li class="pull-left"><a href="" class="facebook icon">Facebook</a></li>
              <li class="pull-left"><a href="" class="twitter icon">Twitter</a></li>
              <li class="pull-left"><a href="" class="instagram icon">Instagram</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!--/.header-->
</div>
<div class="container middle-block-box">
  <div class="middle-blocks">
    <h2 class="coming-soon">New Website coming soon</h2>
    <div class="blocks">
      <div class="col-md-3 block fee">
        <div class="fee_icon"></div>
        <span class="fee_text">Fee Structure</span><a href="#" class="fee-popup-trigger">Click Here</a></div>
      <div class="col-md-3 block admission">
        <div class="mail_icon"></div>
        <span class="mail_text">Admission Details</span><a href="">admmissions@school.ae</a></div>
      <div class="col-md-3 block contact">
        <div class="phone_icon"></div>
        <span class="phone_text">Contact Information</span><span>info@school.ae<br>
        +971 5568688 | +971 44787878</span></div>
      <div class="col-md-3 block locate">
        <div class="map_icon"></div>
        <span class="map_text">Locate Us</span><a href="">Click Here</a></div>
    </div>
  </div>
  <!--middleblcoks-->
</div>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class=""></li>
    <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="2" class=""></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item"><img class="first-slide" src="assets/images/banner1.jpg" alt="First slide"></div>
    <div class="item active"><img class="second-slide" src="assets/images/banner2.jpg" alt="Second slide"></div>
    <div class="item"><img class="third-slide" src="assets/images/banner3.jpg" alt="Third slide"></div>
  </div>
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a></div>
<!--./Carousal-->

<footer class="container">
  <div class="signup">
    <h3 class="signup-title">Sign up we will notify our launch</h3>
    <form class="signup-form">
      <input type="text" class="pull-left" placeholder="My Email here">
      <input type="submit" class="pull-left" >
    </form>
  </div>
</footer>
<div class="modal fade" id="fee-structure-popup" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body"><a class="close" data-dismiss="modal">×</a>
        <h2 class="popup-title">2009/2012 Fees Strcuture</h2>
        <table cellpadding="5" cellspacing="5" class="popup-table table-bordered table-striped">
        <thead>
          <tr class="thead">
            <td><span class="top-title">Grade</span></td>
            <td><span class="top-title">Term1 Due june 23,2009</span></td>
            <td><span class="top-title">Term1 Due june 23,2009</span></td>
            <td><span class="top-title">Term1 Due june 23,2009</span></td>
            <td><span class="top-title">Totals</span></td>
          </tr>
          </thead>
          <tr>
            <td><span class="left-title">PREP</span></td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 24000</td>
          </tr>
          <tr>
            <td><span class="left-title">Grade 2&3</span></td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 24000</td>
          </tr>
          <tr>
            <td><span class="left-title">Grade 3& 4</span></td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 24000</td>
          </tr>
          <tr>
            <td><span class="left-title">Grade 3& 4</span></td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 14000</td>
            <td>AED 24000</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(".fee-popup-trigger").click(function(){
        $('#fee-structure-popup').modal('show');
    });
	
</script>
<body>
</body>
</html>
